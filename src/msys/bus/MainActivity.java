package msys.bus;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;

import msys.bus.db.DBAdapter;
import msys.bus.layout.LayoutUtils;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class MainActivity extends Activity {
	private Calendar calendar = GregorianCalendar.getInstance();
	private SimpleDateFormat dfHm = new SimpleDateFormat("HH:mm",
			Locale.ENGLISH);
	private WorkDayEnum workDay = WorkDayEnum.MON_THU;
	// true only when the bus view is invoked by pressing a button
	private boolean isBusViewVisible = false;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		DBAdapter.init(this);
		// Set the working day
		calendar.setTime(new Date());
		if (calendar.get(Calendar.DAY_OF_WEEK) == Calendar.FRIDAY) {
			workDay = WorkDayEnum.FRIDAY;
		}
		// Display main activity
		setContentView(R.layout.activity_main);
	}

	// Called when the GoToWork button is invoked
	public void onGoToWork(View view) {
		isBusViewVisible = true;
		List<Bus> buses = DBAdapter.getBuses(dfHm.format(new Date()), true, workDay);

		// If more than one bus is available display a new content view
		if (buses.size() > 0
				&& calendar.get(Calendar.DAY_OF_WEEK) != Calendar.SATURDAY
				&& calendar.get(Calendar.DAY_OF_WEEK) != Calendar.SUNDAY) {
			// For each available bus add a corresponding panel to the main view
			setContentView(LayoutUtils.createBusListView(buses, this));
		} else {
			TextView label = new TextView(this);
			label.setText(R.string.no_buses_available);
			setContentView(label);
		}
	}

	// Called when the BackFromWork button is invoked
	public void onBackFromWork(View view) {
		isBusViewVisible = true;
		List<Bus> buses = DBAdapter.getBuses(dfHm.format(new Date()), false, workDay);

		// If more than one bus is available display a new content view
		if (buses.size() > 0
				&& calendar.get(Calendar.DAY_OF_WEEK) != Calendar.SATURDAY
				&& calendar.get(Calendar.DAY_OF_WEEK) != Calendar.SUNDAY) {
			// For each available bus add a corresponding panel to the main view
			setContentView(LayoutUtils.createBusListView(buses, this));
		} else {
			TextView label = new TextView(this);
			label.setText(R.string.no_buses_available);
			setContentView(label);
		}
	}

	// Called when the Timetable button is invoked
	public void onTimetable(View view) {
		Intent timetable = new Intent(this, TimetableActivity.class);
		startActivity(timetable);
	}

	@Override
	public void onBackPressed() {
		if (isBusViewVisible) { // When in BusView return to the mainView
			setContentView(R.layout.activity_main);
			isBusViewVisible = false;
		} else { // When in mainView exit the application
			finish();
			moveTaskToBack(true);
		}
	}

	@Override
	protected void onStop() {
		super.onStop();
		setContentView(R.layout.activity_main);
	}
}
