package msys.bus;

public class Bus {

	private String time;
	private WorkDayEnum workDay;
	private Boolean isApproximate;
	private BusTypeEnum busType;
	private StationEnum startStation;
	private StationEnum endStation;

	public Bus() {}
	
	public Bus(String time, WorkDayEnum workDay, Boolean isApproximate, BusTypeEnum busType,
			StationEnum startStation, StationEnum endStation) {
		this.time = time;
		this.workDay = workDay;
		this.isApproximate = isApproximate;
		this.busType = busType;
		this.startStation = startStation;
		this.endStation = endStation;
	}

	public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}

	public WorkDayEnum getWorkDay() {
		return workDay;
	}

	public void setWorkDay(WorkDayEnum workDay) {
		this.workDay = workDay;
	}

	public Boolean getIsApproximate() {
		return isApproximate;
	}

	public void setIsApproximate(Boolean isApproximate) {
		this.isApproximate = isApproximate;
	}

	public BusTypeEnum getBusType() {
		return busType;
	}

	public void setBusType(BusTypeEnum busType) {
		this.busType = busType;
	}

	public StationEnum getStartStation() {
		return startStation;
	}

	public void setStartStation(StationEnum startStation) {
		this.startStation = startStation;
	}

	public StationEnum getEndStation() {
		return endStation;
	}

	public void setEndStation(StationEnum endStation) {
		this.endStation = endStation;
	}

}
