package msys.bus.layout;

import java.util.List;

import msys.bus.Bus;
import msys.bus.R;
import android.app.Activity;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

public class LayoutUtils {
	//Constructs a View based on a provided list of Buses
	public static View createBusListView(List<Bus> buses, Activity activity) {
		ScrollView mainView = new ScrollView(activity);
		
		//Create the main view which will hold the panels for all the buses
		LinearLayout mainViewGroup = new LinearLayout(activity);
		mainViewGroup.setOrientation(LinearLayout.VERTICAL);
		
		//Create a label explaining what the view is about
		TextView labelMain = new TextView(activity);
		labelMain.setText(R.string.label_main);
		mainViewGroup.addView(labelMain);
		 
		boolean hasApproximateTimeBuses = false;
        for (Bus bus : buses) {
        	mainViewGroup.addView(new BusView(bus, activity));
        	if (bus.getIsApproximate()) {
        		hasApproximateTimeBuses = true;
        	}
		}
		
		//Create a label explaining what texts with * mean if needed
        if (hasApproximateTimeBuses) {
			TextView labelApproximate = new TextView(activity);
			labelApproximate.setText(R.string.label_approximate);
			mainViewGroup.addView(labelApproximate);
        }
        
        mainView.addView(mainViewGroup);        
		return mainView;
	}	
}
