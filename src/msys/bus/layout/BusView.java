package msys.bus.layout;

import msys.bus.Bus;
import msys.bus.BusTypeEnum;
import msys.bus.R;
import android.content.Context;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

public class BusView extends LinearLayout {
		
	public BusView(Bus bus, Context context) {
		super(context);
		
		this.setGravity(Gravity.CENTER_VERTICAL);
		
		//Add the time to the view
		TextView time = new TextView(context);
		time.setText(bus.getTime());
		time.setTextSize(TypedValue.COMPLEX_UNIT_SP, 28);
		this.addView(time);
		
		//Add bus type
		ImageView busType = new ImageView(context);
		busType.setImageResource(R.drawable.ic_launcher);
		if (bus.getBusType().equals(BusTypeEnum.BIG_BUS)) {
			busType.setImageResource(R.drawable.big_bus);
		} else {
			busType.setImageResource(R.drawable.mini_bus);
		}
		busType.setVisibility(View.VISIBLE);
		busType.setPadding(5, 2, 5, 2);
		this.addView(busType);
		
		//Add legend
		String startStation = bus.getStartStation().name().replace("_", " ");
		String endStation = bus.getEndStation().name().replace("_", " ");
		TextView legend = new TextView(context);
		legend.setText(context.getString(R.string.legend_part_1)
				+ " " + startStation + " " + context.getString(R.string.legend_part_2)+ " " + endStation
				+ (bus.getIsApproximate() ? " *" : ""));
		this.addView(legend);
	}

}
