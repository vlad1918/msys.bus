package msys.bus.db;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;
import msys.bus.Bus;
import msys.bus.BusTypeEnum;
import msys.bus.StationEnum;
import msys.bus.WorkDayEnum;

public class DBAdapter {

	/******* if debug is set true then it will show all Logcat message ***/
	public static final boolean DEBUG = false;

	/********** Logcat TAG ************/
	public static final String LOG_TAG = "DBAdapter";

	/************ Table Fields ************/
	public static final String TABLE_NAME = "bus";
	public static final String COLUMN_NAME_TIME = "time";
	public static final String COLUMN_NAME_WORK_DAY = "work_day";
	public static final String COLUMN_NAME_IS_APPROXIMATE = "is_approximate";
	public static final String COLUMN_NAME_BUS_TYPE = "bus_type";
	public static final String COLUMN_NAME_START_STATION = "start_station";
	public static final String COLUMN_NAME_END_STATION = "end_Station";

	private static final String TYPE_TEXT = " TEXT";
	private static final String TYPE_INT = " INTEGER";
	private static final String COMMA_SEP = ",";

	/************* Database Name ************/
	public static final String DATABASE_NAME = "msys.bus.db";

	/**** Database Version (Increase one if want to also upgrade your database) ****/
	public static final int DATABASE_VERSION = 5;// started at 1

	/** Create table syntax */
	public static final String SQL_CREATE_TABLE = "CREATE TABLE "
			+ TABLE_NAME
			+ " (" // + BusEntry._ID + " INTEGER PRIMARY KEY,"
			+ COLUMN_NAME_TIME + TYPE_TEXT + COMMA_SEP + COLUMN_NAME_WORK_DAY
			+ TYPE_TEXT + COMMA_SEP + COLUMN_NAME_IS_APPROXIMATE + TYPE_INT + COMMA_SEP
			+ COLUMN_NAME_BUS_TYPE + TYPE_TEXT
			+ COMMA_SEP + COLUMN_NAME_START_STATION + TYPE_TEXT + COMMA_SEP
			+ COLUMN_NAME_END_STATION + TYPE_TEXT + " )";

	public static final String SQL_DROP_TABLE = "DROP TABLE IF EXISTS "
			+ TABLE_NAME;

	/********* Used to open database in syncronized way *********/
	private static DataBaseHelper DBHelper = null;

	protected DBAdapter() {

	}

	/********** Initialize database *********/
	public static void init(Context context) {
		if (DBHelper == null) {
			if (DEBUG)
				Log.i("DBAdapter", context.toString());
			DBHelper = new DataBaseHelper(context);
		}
	}

	/********** Main Database creation INNER class ********/
	private static class DataBaseHelper extends SQLiteOpenHelper {
		public DataBaseHelper(Context context) {
			super(context, DATABASE_NAME, null, DATABASE_VERSION);
		}

		@Override
		public void onCreate(SQLiteDatabase db) {
			if (DEBUG)
				Log.i(LOG_TAG, "new create");
			try {
				db.execSQL(SQL_CREATE_TABLE);
				
				//Insert data in the newly created table too
				
				//Buses to work MON-THU
				DBAdapter.addBus(db, new Bus("06:40", WorkDayEnum.MON_THU, false, BusTypeEnum.MINI_BUS, StationEnum.AUREL_VLAICU, StationEnum.METRO_SYSTEMS));			
				DBAdapter.addBus(db, new Bus("07:05", WorkDayEnum.MON_THU, false, BusTypeEnum.MINI_BUS, StationEnum.AUREL_VLAICU, StationEnum.METRO_SYSTEMS));	
				DBAdapter.addBus(db, new Bus("07:30", WorkDayEnum.MON_THU, false, BusTypeEnum.MINI_BUS, StationEnum.AUREL_VLAICU, StationEnum.METRO_SYSTEMS));	
				DBAdapter.addBus(db, new Bus("07:45", WorkDayEnum.MON_THU, false, BusTypeEnum.BIG_BUS, StationEnum.AUREL_VLAICU, StationEnum.METRO_SYSTEMS));	
				DBAdapter.addBus(db, new Bus("08:00", WorkDayEnum.MON_THU, false, BusTypeEnum.MINI_BUS, StationEnum.AUREL_VLAICU, StationEnum.METRO_SYSTEMS));			
				DBAdapter.addBus(db, new Bus("08:10", WorkDayEnum.MON_THU, false, BusTypeEnum.BIG_BUS, StationEnum.AUREL_VLAICU, StationEnum.METRO_SYSTEMS));	
				DBAdapter.addBus(db, new Bus("08:20", WorkDayEnum.MON_THU, false, BusTypeEnum.MINI_BUS, StationEnum.AUREL_VLAICU, StationEnum.METRO_SYSTEMS));	
				DBAdapter.addBus(db, new Bus("08:30", WorkDayEnum.MON_THU, false, BusTypeEnum.BIG_BUS, StationEnum.AUREL_VLAICU, StationEnum.METRO_SYSTEMS));
				DBAdapter.addBus(db, new Bus("08:30", WorkDayEnum.MON_THU, false, BusTypeEnum.MINI_BUS, StationEnum.PIPERA, StationEnum.METRO_SYSTEMS));
				DBAdapter.addBus(db, new Bus("08:40", WorkDayEnum.MON_THU, false, BusTypeEnum.MINI_BUS, StationEnum.AUREL_VLAICU, StationEnum.METRO_SYSTEMS));	
				DBAdapter.addBus(db, new Bus("08:50", WorkDayEnum.MON_THU, false, BusTypeEnum.BIG_BUS, StationEnum.AUREL_VLAICU, StationEnum.METRO_SYSTEMS));	
				DBAdapter.addBus(db, new Bus("09:00", WorkDayEnum.MON_THU, false, BusTypeEnum.MINI_BUS, StationEnum.AUREL_VLAICU, StationEnum.METRO_SYSTEMS));
				DBAdapter.addBus(db, new Bus("09:10", WorkDayEnum.MON_THU, false, BusTypeEnum.MINI_BUS, StationEnum.AUREL_VLAICU, StationEnum.METRO_SYSTEMS));	
				DBAdapter.addBus(db, new Bus("09:20", WorkDayEnum.MON_THU, false, BusTypeEnum.BIG_BUS, StationEnum.AUREL_VLAICU, StationEnum.METRO_SYSTEMS));	
				DBAdapter.addBus(db, new Bus("09:30", WorkDayEnum.MON_THU, false, BusTypeEnum.MINI_BUS, StationEnum.AUREL_VLAICU, StationEnum.METRO_SYSTEMS));
				DBAdapter.addBus(db, new Bus("09:30", WorkDayEnum.MON_THU, false, BusTypeEnum.MINI_BUS, StationEnum.PIPERA, StationEnum.METRO_SYSTEMS));
				DBAdapter.addBus(db, new Bus("09:45", WorkDayEnum.MON_THU, false, BusTypeEnum.BIG_BUS, StationEnum.AUREL_VLAICU, StationEnum.METRO_SYSTEMS));	
				DBAdapter.addBus(db, new Bus("10:45", WorkDayEnum.MON_THU, false, BusTypeEnum.MINI_BUS, StationEnum.AUREL_VLAICU, StationEnum.METRO_SYSTEMS));	
				DBAdapter.addBus(db, new Bus("13:45", WorkDayEnum.MON_THU, false, BusTypeEnum.MINI_BUS, StationEnum.PIPERA, StationEnum.METRO_SYSTEMS));
				DBAdapter.addBus(db, new Bus("15:30", WorkDayEnum.MON_THU, true, BusTypeEnum.MINI_BUS, StationEnum.PIPERA, StationEnum.METRO_SYSTEMS));
				DBAdapter.addBus(db, new Bus("16:15", WorkDayEnum.MON_THU, true, BusTypeEnum.MINI_BUS, StationEnum.PIPERA, StationEnum.METRO_SYSTEMS));
				DBAdapter.addBus(db, new Bus("16:45", WorkDayEnum.MON_THU, true, BusTypeEnum.MINI_BUS, StationEnum.PIPERA, StationEnum.METRO_SYSTEMS));
				DBAdapter.addBus(db, new Bus("17:25", WorkDayEnum.MON_THU, true, BusTypeEnum.BIG_BUS, StationEnum.PIPERA, StationEnum.METRO_SYSTEMS));
				DBAdapter.addBus(db, new Bus("17:45", WorkDayEnum.MON_THU, true, BusTypeEnum.MINI_BUS, StationEnum.PIPERA, StationEnum.METRO_SYSTEMS));
				DBAdapter.addBus(db, new Bus("18:00", WorkDayEnum.MON_THU, true, BusTypeEnum.MINI_BUS, StationEnum.PIPERA, StationEnum.METRO_SYSTEMS));
				DBAdapter.addBus(db, new Bus("18:15", WorkDayEnum.MON_THU, true, BusTypeEnum.MINI_BUS, StationEnum.PIPERA, StationEnum.METRO_SYSTEMS));
				DBAdapter.addBus(db, new Bus("18:30", WorkDayEnum.MON_THU, true, BusTypeEnum.BIG_BUS, StationEnum.PIPERA, StationEnum.METRO_SYSTEMS));
				DBAdapter.addBus(db, new Bus("18:45", WorkDayEnum.MON_THU, true, BusTypeEnum.MINI_BUS, StationEnum.PIPERA, StationEnum.METRO_SYSTEMS));
				DBAdapter.addBus(db, new Bus("19:00", WorkDayEnum.MON_THU, true, BusTypeEnum.MINI_BUS, StationEnum.PIPERA, StationEnum.METRO_SYSTEMS));
				DBAdapter.addBus(db, new Bus("19:15", WorkDayEnum.MON_THU, true, BusTypeEnum.MINI_BUS, StationEnum.AUREL_VLAICU, StationEnum.METRO_SYSTEMS));	
				DBAdapter.addBus(db, new Bus("19:30", WorkDayEnum.MON_THU, true, BusTypeEnum.MINI_BUS, StationEnum.AUREL_VLAICU, StationEnum.METRO_SYSTEMS));
				DBAdapter.addBus(db, new Bus("20:00", WorkDayEnum.MON_THU, true, BusTypeEnum.MINI_BUS, StationEnum.AUREL_VLAICU, StationEnum.METRO_SYSTEMS));
				DBAdapter.addBus(db, new Bus("20:30", WorkDayEnum.MON_THU, true, BusTypeEnum.MINI_BUS, StationEnum.AUREL_VLAICU, StationEnum.METRO_SYSTEMS));
				DBAdapter.addBus(db, new Bus("21:00", WorkDayEnum.MON_THU, true, BusTypeEnum.MINI_BUS, StationEnum.AUREL_VLAICU, StationEnum.METRO_SYSTEMS));
				
				//Buses to work FRIDAY
				DBAdapter.addBus(db, new Bus("06:40", WorkDayEnum.FRIDAY, false, BusTypeEnum.MINI_BUS, StationEnum.AUREL_VLAICU, StationEnum.METRO_SYSTEMS));			
				DBAdapter.addBus(db, new Bus("07:05", WorkDayEnum.FRIDAY, false, BusTypeEnum.MINI_BUS, StationEnum.AUREL_VLAICU, StationEnum.METRO_SYSTEMS));	
				DBAdapter.addBus(db, new Bus("07:30", WorkDayEnum.FRIDAY, false, BusTypeEnum.MINI_BUS, StationEnum.AUREL_VLAICU, StationEnum.METRO_SYSTEMS));	
				DBAdapter.addBus(db, new Bus("07:45", WorkDayEnum.FRIDAY, false, BusTypeEnum.BIG_BUS, StationEnum.AUREL_VLAICU, StationEnum.METRO_SYSTEMS));	
				DBAdapter.addBus(db, new Bus("08:00", WorkDayEnum.FRIDAY, false, BusTypeEnum.MINI_BUS, StationEnum.AUREL_VLAICU, StationEnum.METRO_SYSTEMS));			
				DBAdapter.addBus(db, new Bus("08:10", WorkDayEnum.FRIDAY, false, BusTypeEnum.BIG_BUS, StationEnum.AUREL_VLAICU, StationEnum.METRO_SYSTEMS));	
				DBAdapter.addBus(db, new Bus("08:20", WorkDayEnum.FRIDAY, false, BusTypeEnum.MINI_BUS, StationEnum.AUREL_VLAICU, StationEnum.METRO_SYSTEMS));	
				DBAdapter.addBus(db, new Bus("08:30", WorkDayEnum.FRIDAY, false, BusTypeEnum.BIG_BUS, StationEnum.AUREL_VLAICU, StationEnum.METRO_SYSTEMS));
				DBAdapter.addBus(db, new Bus("08:30", WorkDayEnum.FRIDAY, false, BusTypeEnum.MINI_BUS, StationEnum.PIPERA, StationEnum.METRO_SYSTEMS));
				DBAdapter.addBus(db, new Bus("08:40", WorkDayEnum.FRIDAY, false, BusTypeEnum.MINI_BUS, StationEnum.AUREL_VLAICU, StationEnum.METRO_SYSTEMS));	
				DBAdapter.addBus(db, new Bus("08:50", WorkDayEnum.FRIDAY, false, BusTypeEnum.BIG_BUS, StationEnum.AUREL_VLAICU, StationEnum.METRO_SYSTEMS));	
				DBAdapter.addBus(db, new Bus("09:00", WorkDayEnum.FRIDAY, false, BusTypeEnum.MINI_BUS, StationEnum.AUREL_VLAICU, StationEnum.METRO_SYSTEMS));
				DBAdapter.addBus(db, new Bus("09:10", WorkDayEnum.FRIDAY, false, BusTypeEnum.MINI_BUS, StationEnum.AUREL_VLAICU, StationEnum.METRO_SYSTEMS));	
				DBAdapter.addBus(db, new Bus("09:20", WorkDayEnum.FRIDAY, false, BusTypeEnum.BIG_BUS, StationEnum.AUREL_VLAICU, StationEnum.METRO_SYSTEMS));	
				DBAdapter.addBus(db, new Bus("09:30", WorkDayEnum.FRIDAY, false, BusTypeEnum.MINI_BUS, StationEnum.AUREL_VLAICU, StationEnum.METRO_SYSTEMS));
				DBAdapter.addBus(db, new Bus("09:30", WorkDayEnum.FRIDAY, false, BusTypeEnum.MINI_BUS, StationEnum.PIPERA, StationEnum.METRO_SYSTEMS));
				DBAdapter.addBus(db, new Bus("09:45", WorkDayEnum.FRIDAY, false, BusTypeEnum.BIG_BUS, StationEnum.AUREL_VLAICU, StationEnum.METRO_SYSTEMS));	
				DBAdapter.addBus(db, new Bus("10:45", WorkDayEnum.FRIDAY, false, BusTypeEnum.MINI_BUS, StationEnum.AUREL_VLAICU, StationEnum.METRO_SYSTEMS));	
				DBAdapter.addBus(db, new Bus("13:45", WorkDayEnum.FRIDAY, true, BusTypeEnum.BIG_BUS, StationEnum.PIPERA, StationEnum.METRO_SYSTEMS));
				DBAdapter.addBus(db, new Bus("15:00", WorkDayEnum.FRIDAY, true, BusTypeEnum.BIG_BUS, StationEnum.PIPERA, StationEnum.METRO_SYSTEMS));
				DBAdapter.addBus(db, new Bus("15:45", WorkDayEnum.FRIDAY, true, BusTypeEnum.BIG_BUS, StationEnum.PIPERA, StationEnum.METRO_SYSTEMS));
				DBAdapter.addBus(db, new Bus("16:30", WorkDayEnum.FRIDAY, true, BusTypeEnum.MINI_BUS, StationEnum.PIPERA, StationEnum.METRO_SYSTEMS));
				DBAdapter.addBus(db, new Bus("17:30", WorkDayEnum.FRIDAY, true, BusTypeEnum.MINI_BUS, StationEnum.PIPERA, StationEnum.METRO_SYSTEMS));
				DBAdapter.addBus(db, new Bus("18:00", WorkDayEnum.FRIDAY, true, BusTypeEnum.MINI_BUS, StationEnum.PIPERA, StationEnum.METRO_SYSTEMS));
				DBAdapter.addBus(db, new Bus("18:45", WorkDayEnum.FRIDAY, true, BusTypeEnum.MINI_BUS, StationEnum.PIPERA, StationEnum.METRO_SYSTEMS));				
				DBAdapter.addBus(db, new Bus("19:45", WorkDayEnum.FRIDAY, true, BusTypeEnum.MINI_BUS, StationEnum.AUREL_VLAICU, StationEnum.METRO_SYSTEMS));
				
				//Buses from work MON-THU
				DBAdapter.addBus(db, new Bus("06:55", WorkDayEnum.MON_THU, true, BusTypeEnum.MINI_BUS, StationEnum.METRO_SYSTEMS, StationEnum.AUREL_VLAICU));
				DBAdapter.addBus(db, new Bus("07:20", WorkDayEnum.MON_THU, true, BusTypeEnum.MINI_BUS, StationEnum.METRO_SYSTEMS, StationEnum.AUREL_VLAICU));
				DBAdapter.addBus(db, new Bus("07:30", WorkDayEnum.MON_THU, true, BusTypeEnum.BIG_BUS, StationEnum.METRO_SYSTEMS, StationEnum.AUREL_VLAICU));
				DBAdapter.addBus(db, new Bus("07:50", WorkDayEnum.MON_THU, true, BusTypeEnum.MINI_BUS, StationEnum.METRO_SYSTEMS, StationEnum.AUREL_VLAICU));
				DBAdapter.addBus(db, new Bus("08:00", WorkDayEnum.MON_THU, true, BusTypeEnum.MINI_BUS, StationEnum.METRO_SYSTEMS, StationEnum.AUREL_VLAICU));
				DBAdapter.addBus(db, new Bus("08:10", WorkDayEnum.MON_THU, true, BusTypeEnum.MINI_BUS, StationEnum.METRO_SYSTEMS, StationEnum.AUREL_VLAICU));
				DBAdapter.addBus(db, new Bus("08:20", WorkDayEnum.MON_THU, true, BusTypeEnum.MINI_BUS, StationEnum.METRO_SYSTEMS, StationEnum.AUREL_VLAICU));
				DBAdapter.addBus(db, new Bus("08:20", WorkDayEnum.MON_THU, true, BusTypeEnum.MINI_BUS, StationEnum.METRO_SYSTEMS, StationEnum.PIPERA));
				DBAdapter.addBus(db, new Bus("08:30", WorkDayEnum.MON_THU, true, BusTypeEnum.MINI_BUS, StationEnum.METRO_SYSTEMS, StationEnum.AUREL_VLAICU));
				DBAdapter.addBus(db, new Bus("08:40", WorkDayEnum.MON_THU, true, BusTypeEnum.MINI_BUS, StationEnum.METRO_SYSTEMS, StationEnum.AUREL_VLAICU));
				DBAdapter.addBus(db, new Bus("08:50", WorkDayEnum.MON_THU, true, BusTypeEnum.MINI_BUS, StationEnum.METRO_SYSTEMS, StationEnum.AUREL_VLAICU));
				DBAdapter.addBus(db, new Bus("09:00", WorkDayEnum.MON_THU, true, BusTypeEnum.MINI_BUS, StationEnum.METRO_SYSTEMS, StationEnum.AUREL_VLAICU));
				DBAdapter.addBus(db, new Bus("09:10", WorkDayEnum.MON_THU, true, BusTypeEnum.MINI_BUS, StationEnum.METRO_SYSTEMS, StationEnum.AUREL_VLAICU));
				DBAdapter.addBus(db, new Bus("09:20", WorkDayEnum.MON_THU, true, BusTypeEnum.MINI_BUS, StationEnum.METRO_SYSTEMS, StationEnum.AUREL_VLAICU));
				DBAdapter.addBus(db, new Bus("09:20", WorkDayEnum.MON_THU, true, BusTypeEnum.MINI_BUS, StationEnum.METRO_SYSTEMS, StationEnum.PIPERA));
				DBAdapter.addBus(db, new Bus("09:30", WorkDayEnum.MON_THU, true, BusTypeEnum.MINI_BUS, StationEnum.METRO_SYSTEMS, StationEnum.AUREL_VLAICU));
				DBAdapter.addBus(db, new Bus("10:30", WorkDayEnum.MON_THU, true, BusTypeEnum.MINI_BUS, StationEnum.METRO_SYSTEMS, StationEnum.AUREL_VLAICU));
				DBAdapter.addBus(db, new Bus("13:30", WorkDayEnum.MON_THU, false, BusTypeEnum.MINI_BUS, StationEnum.METRO_SYSTEMS, StationEnum.PIPERA));
				DBAdapter.addBus(db, new Bus("15:15", WorkDayEnum.MON_THU, false, BusTypeEnum.MINI_BUS, StationEnum.METRO_SYSTEMS, StationEnum.PIPERA));
				DBAdapter.addBus(db, new Bus("16:00", WorkDayEnum.MON_THU, false, BusTypeEnum.MINI_BUS, StationEnum.METRO_SYSTEMS, StationEnum.PIPERA));
				DBAdapter.addBus(db, new Bus("16:30", WorkDayEnum.MON_THU, false, BusTypeEnum.MINI_BUS, StationEnum.METRO_SYSTEMS, StationEnum.PIPERA));
				DBAdapter.addBus(db, new Bus("17:10", WorkDayEnum.MON_THU, false, BusTypeEnum.BIG_BUS, StationEnum.METRO_SYSTEMS, StationEnum.PIPERA));
				DBAdapter.addBus(db, new Bus("17:30", WorkDayEnum.MON_THU, false, BusTypeEnum.MINI_BUS, StationEnum.METRO_SYSTEMS, StationEnum.PIPERA));
				DBAdapter.addBus(db, new Bus("17:45", WorkDayEnum.MON_THU, false, BusTypeEnum.MINI_BUS, StationEnum.METRO_SYSTEMS, StationEnum.PIPERA));
				DBAdapter.addBus(db, new Bus("18:00", WorkDayEnum.MON_THU, false, BusTypeEnum.MINI_BUS, StationEnum.METRO_SYSTEMS, StationEnum.PIPERA));								
	        	DBAdapter.addBus(db, new Bus("18:15", WorkDayEnum.MON_THU, false, BusTypeEnum.BIG_BUS, StationEnum.METRO_SYSTEMS, StationEnum.PIPERA));
	        	DBAdapter.addBus(db, new Bus("18:30", WorkDayEnum.MON_THU, false, BusTypeEnum.MINI_BUS, StationEnum.METRO_SYSTEMS, StationEnum.PIPERA));
	        	DBAdapter.addBus(db, new Bus("18:45", WorkDayEnum.MON_THU, false, BusTypeEnum.MINI_BUS, StationEnum.METRO_SYSTEMS, StationEnum.PIPERA));
	        	DBAdapter.addBus(db, new Bus("19:00", WorkDayEnum.MON_THU, false, BusTypeEnum.MINI_BUS, StationEnum.METRO_SYSTEMS, StationEnum.AUREL_VLAICU));
	        	DBAdapter.addBus(db, new Bus("19:15", WorkDayEnum.MON_THU, false, BusTypeEnum.MINI_BUS, StationEnum.METRO_SYSTEMS, StationEnum.AUREL_VLAICU));
	        	DBAdapter.addBus(db, new Bus("19:30", WorkDayEnum.MON_THU, false, BusTypeEnum.MINI_BUS, StationEnum.METRO_SYSTEMS, StationEnum.AUREL_VLAICU));
	        	DBAdapter.addBus(db, new Bus("19:45", WorkDayEnum.MON_THU, false, BusTypeEnum.MINI_BUS, StationEnum.METRO_SYSTEMS, StationEnum.AUREL_VLAICU));
	        	DBAdapter.addBus(db, new Bus("20:15", WorkDayEnum.MON_THU, false, BusTypeEnum.MINI_BUS, StationEnum.METRO_SYSTEMS, StationEnum.AUREL_VLAICU));
	        	DBAdapter.addBus(db, new Bus("20:45", WorkDayEnum.MON_THU, false, BusTypeEnum.MINI_BUS, StationEnum.METRO_SYSTEMS, StationEnum.AUREL_VLAICU));
	        	
	        	//Buses from work FRIDAY
				DBAdapter.addBus(db, new Bus("06:55", WorkDayEnum.FRIDAY, true, BusTypeEnum.MINI_BUS, StationEnum.METRO_SYSTEMS, StationEnum.AUREL_VLAICU));
				DBAdapter.addBus(db, new Bus("07:20", WorkDayEnum.FRIDAY, true, BusTypeEnum.MINI_BUS, StationEnum.METRO_SYSTEMS, StationEnum.AUREL_VLAICU));
				DBAdapter.addBus(db, new Bus("07:30", WorkDayEnum.FRIDAY, true, BusTypeEnum.BIG_BUS, StationEnum.METRO_SYSTEMS, StationEnum.AUREL_VLAICU));
				DBAdapter.addBus(db, new Bus("07:50", WorkDayEnum.FRIDAY, true, BusTypeEnum.MINI_BUS, StationEnum.METRO_SYSTEMS, StationEnum.AUREL_VLAICU));
				DBAdapter.addBus(db, new Bus("08:00", WorkDayEnum.FRIDAY, true, BusTypeEnum.MINI_BUS, StationEnum.METRO_SYSTEMS, StationEnum.AUREL_VLAICU));
				DBAdapter.addBus(db, new Bus("08:10", WorkDayEnum.FRIDAY, true, BusTypeEnum.MINI_BUS, StationEnum.METRO_SYSTEMS, StationEnum.AUREL_VLAICU));
				DBAdapter.addBus(db, new Bus("08:20", WorkDayEnum.FRIDAY, true, BusTypeEnum.MINI_BUS, StationEnum.METRO_SYSTEMS, StationEnum.AUREL_VLAICU));
				DBAdapter.addBus(db, new Bus("08:20", WorkDayEnum.FRIDAY, true, BusTypeEnum.MINI_BUS, StationEnum.METRO_SYSTEMS, StationEnum.PIPERA));
				DBAdapter.addBus(db, new Bus("08:30", WorkDayEnum.FRIDAY, true, BusTypeEnum.MINI_BUS, StationEnum.METRO_SYSTEMS, StationEnum.AUREL_VLAICU));
				DBAdapter.addBus(db, new Bus("08:40", WorkDayEnum.FRIDAY, true, BusTypeEnum.MINI_BUS, StationEnum.METRO_SYSTEMS, StationEnum.AUREL_VLAICU));
				DBAdapter.addBus(db, new Bus("08:50", WorkDayEnum.FRIDAY, true, BusTypeEnum.MINI_BUS, StationEnum.METRO_SYSTEMS, StationEnum.AUREL_VLAICU));
				DBAdapter.addBus(db, new Bus("09:00", WorkDayEnum.FRIDAY, true, BusTypeEnum.MINI_BUS, StationEnum.METRO_SYSTEMS, StationEnum.AUREL_VLAICU));
				DBAdapter.addBus(db, new Bus("09:10", WorkDayEnum.FRIDAY, true, BusTypeEnum.MINI_BUS, StationEnum.METRO_SYSTEMS, StationEnum.AUREL_VLAICU));
				DBAdapter.addBus(db, new Bus("09:20", WorkDayEnum.FRIDAY, true, BusTypeEnum.MINI_BUS, StationEnum.METRO_SYSTEMS, StationEnum.AUREL_VLAICU));
				DBAdapter.addBus(db, new Bus("09:20", WorkDayEnum.FRIDAY, true, BusTypeEnum.MINI_BUS, StationEnum.METRO_SYSTEMS, StationEnum.PIPERA));
				DBAdapter.addBus(db, new Bus("09:30", WorkDayEnum.FRIDAY, true, BusTypeEnum.MINI_BUS, StationEnum.METRO_SYSTEMS, StationEnum.AUREL_VLAICU));
				DBAdapter.addBus(db, new Bus("10:30", WorkDayEnum.FRIDAY, true, BusTypeEnum.MINI_BUS, StationEnum.METRO_SYSTEMS, StationEnum.AUREL_VLAICU));	        	
				DBAdapter.addBus(db, new Bus("13:30", WorkDayEnum.FRIDAY, false, BusTypeEnum.BIG_BUS, StationEnum.METRO_SYSTEMS, StationEnum.PIPERA));
				DBAdapter.addBus(db, new Bus("14:40", WorkDayEnum.FRIDAY, false, BusTypeEnum.BIG_BUS, StationEnum.METRO_SYSTEMS, StationEnum.PIPERA));							
	        	DBAdapter.addBus(db, new Bus("15:30", WorkDayEnum.FRIDAY, false, BusTypeEnum.BIG_BUS, StationEnum.METRO_SYSTEMS, StationEnum.PIPERA));
	        	DBAdapter.addBus(db, new Bus("16:15", WorkDayEnum.FRIDAY, false, BusTypeEnum.MINI_BUS, StationEnum.METRO_SYSTEMS, StationEnum.PIPERA));
	        	DBAdapter.addBus(db, new Bus("17:15", WorkDayEnum.FRIDAY, false, BusTypeEnum.MINI_BUS, StationEnum.METRO_SYSTEMS, StationEnum.PIPERA));
	        	DBAdapter.addBus(db, new Bus("17:45", WorkDayEnum.FRIDAY, false, BusTypeEnum.MINI_BUS, StationEnum.METRO_SYSTEMS, StationEnum.PIPERA));
	        	DBAdapter.addBus(db, new Bus("18:30", WorkDayEnum.FRIDAY, false, BusTypeEnum.MINI_BUS, StationEnum.METRO_SYSTEMS, StationEnum.PIPERA));
	        	DBAdapter.addBus(db, new Bus("19:15", WorkDayEnum.FRIDAY, false, BusTypeEnum.MINI_BUS, StationEnum.METRO_SYSTEMS, StationEnum.AUREL_VLAICU));
	        	DBAdapter.addBus(db, new Bus("20:15", WorkDayEnum.FRIDAY, false, BusTypeEnum.MINI_BUS, StationEnum.METRO_SYSTEMS, StationEnum.AUREL_VLAICU));	        
	        		        	
			} catch (Exception exception) {
				if (DEBUG)
					Log.i(LOG_TAG, "Exception onCreate() exception");
			}
		}

		@Override
		public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
			if (DEBUG)
				Log.w(LOG_TAG, "Upgrading database from version" + oldVersion
						+ "to" + newVersion + "...");

			db.execSQL(SQL_DROP_TABLE);
			onCreate(db);
		}

	} // Inner class closed

	/***** Open database for insert,update,delete in syncronized manner ****/
	private static synchronized SQLiteDatabase open() throws SQLException {
		return DBHelper.getWritableDatabase();
	}

	/************* Data functions *************/
	public static void addBus(SQLiteDatabase db, Bus bus) {

		// Open database for Read / Write
//		final SQLiteDatabase db = DBHelper.getWritableDatabase();

		String time = bus.getTime();
		String workDay = bus.getWorkDay().name();
		Integer isApproximate = bus.getIsApproximate() ? 1 : 0;
		String busType = bus.getBusType().name();
		String startStation = bus.getStartStation().name();
		String endStation = bus.getEndStation().name();

		ContentValues cVal = new ContentValues();

		cVal.put(COLUMN_NAME_TIME, time);
		cVal.put(COLUMN_NAME_WORK_DAY, workDay);
		cVal.put(COLUMN_NAME_IS_APPROXIMATE, isApproximate);
		cVal.put(COLUMN_NAME_BUS_TYPE, busType);
		cVal.put(COLUMN_NAME_START_STATION, startStation);
		cVal.put(COLUMN_NAME_END_STATION, endStation);

		// Insert user values in database
		db.insert(TABLE_NAME, null, cVal);
//		db.close(); // Closing database connection
	}

	// Gets all buses that need to leave after the
	public static List<Bus> getBuses(String currentTime, boolean goingToWork, WorkDayEnum workDay) {

		List<Bus> busList = new ArrayList<Bus>();
		
		//Set the startStation clause
		String startStationClause;
		if (goingToWork) {
			startStationClause = COLUMN_NAME_START_STATION + " <> 'METRO_SYSTEMS'";
		} else {
			startStationClause = COLUMN_NAME_START_STATION + " = 'METRO_SYSTEMS'";
		}

		// Select All Query
		String selectQuery = "SELECT " + COLUMN_NAME_TIME + ", " + COLUMN_NAME_WORK_DAY + ", " 
				+ COLUMN_NAME_IS_APPROXIMATE + ", " + COLUMN_NAME_BUS_TYPE + ", " + COLUMN_NAME_START_STATION 
				+ ", " + COLUMN_NAME_END_STATION + " FROM "
				+ TABLE_NAME + " WHERE " + COLUMN_NAME_TIME + " > '" + currentTime + "' AND "+ startStationClause
				+ "AND " + COLUMN_NAME_WORK_DAY + "='" + workDay.name() + "'";

		// Open database for Read / Write
		final SQLiteDatabase db = open();
		Cursor cursor = db.rawQuery(selectQuery, null);

		// looping through all rows and adding to list
		if (cursor.moveToFirst()) {
			do {

				Bus bus = new Bus();
				bus.setTime(cursor.getString(0));
				bus.setWorkDay(WorkDayEnum.valueOf(cursor.getString(1)));
				bus.setIsApproximate(cursor.getInt(2) != 0);
				bus.setBusType(BusTypeEnum.valueOf(cursor.getString(3)));
				bus.setStartStation(StationEnum.valueOf(cursor.getString(4)));
				bus.setEndStation(StationEnum.valueOf(cursor.getString(5)));

				// Adding contact to list
				busList.add(bus);
			} while (cursor.moveToNext());
		}

		// return bus list
		return busList;
	}
}