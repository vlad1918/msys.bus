package msys.bus;

import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import java.util.List;
import msys.bus.db.DBAdapter;
import msys.bus.layout.LayoutUtils;
import android.content.Intent;
import android.view.View;

public class TimetableActivity extends ActionBarActivity {

	// true only when the bus view is invoked by pressing a button
	private boolean isBusViewVisible = false;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		//DBAdapter.init(this);
		// Display main activity
		setContentView(R.layout.activity_timetable);
	}

	// Called when the GoToWorkMonThu button is invoked
	public void onGoToWorkMonThu(View view) {
		isBusViewVisible = true;
		List<Bus> buses = DBAdapter.getBuses("00:01", true, WorkDayEnum.MON_THU);
		// For each available bus add a corresponding panel to the main view
		setContentView(LayoutUtils.createBusListView(buses, this));
	}

	// Called when the GoToWorkFri button is invoked
	public void onGoToWorkFri(View view) {
		isBusViewVisible = true;
		List<Bus> buses = DBAdapter.getBuses("00:01", true, WorkDayEnum.FRIDAY);
		// For each available bus add a corresponding panel to the main view
		setContentView(LayoutUtils.createBusListView(buses, this));
	}

	// Called when the BackFromWorkMonThu button is invoked
	public void onBackFromWorkMonThu(View view) {
		isBusViewVisible = true;
		List<Bus> buses = DBAdapter.getBuses("00:01", false, WorkDayEnum.MON_THU);
		// For each available bus add a corresponding panel to the main view
		setContentView(LayoutUtils.createBusListView(buses, this));
	}

	// Called when the BackFromWorkFri button is invoked
	public void onBackFromWorkFri(View view) {
		isBusViewVisible = true;
		List<Bus> buses = DBAdapter.getBuses("00:01", false, WorkDayEnum.FRIDAY);
		// For each available bus add a corresponding panel to the main view
		setContentView(LayoutUtils.createBusListView(buses, this));
	}

	@Override
	public void onBackPressed() {
		if (isBusViewVisible) { // When in BusView return to the mainView
			setContentView(R.layout.activity_timetable);
			isBusViewVisible = false;
		} else { // When in mainView go to MainActivity
			finish();
			Intent main = new Intent(this, MainActivity.class);
			startActivity(main);
		}
	}

	@Override
	protected void onStop() {
		super.onStop();
			Intent main = new Intent(this, MainActivity.class);
			startActivity(main);
	}
}
